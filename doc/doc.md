
# Projekt BOA

```mermaid
graph LR
MAP --> POINTS --> PATHS --> SOLVE
```

### MAP
1. Rastorowa mapa o wymiarach n x m opisująca wysokość.
2. Wyświetlanie:
	- Zapis do pliku img
	- Wysokość przedstawiona kolorem
	- Legenda wysokości
3. Możliwośc dodania i wyświetlania punktów
4. Możliwośc dodania i wyświetlania ścieżek
5. Wyróżnienie optymalnej śćieżki
6. Opcjonalnie: Pobierać png i na podstawie kolorów (poziom szarośći) wyznaczyć wysokość i stworzyć mapę.

### POINTS
1. Generowanie losowe punktów na mapie
2. Ilość punktów N jako parametr
3. Zapis punktów jako wektor struktury "point":
	- x, y - pozycja

### PATHS

```mermaid
graph LR
NEIGHBOURS --> H-TRACE --> WEIGHT
```
#### NEIGHBOURS
1. Wyznaczenie ścieżek za pomocą metody k najbliższych sąsiadów
2. Zapis ścieżek jako wektor struktury "path":
	- beg, end - punkty
3. Ilość najbliższych sasiadów k, jako parametr
#### H-TRACE
1. Wyznaczenie wektora jednostkowego ścieżki
2. Oblicznienie punktów na ścieżce i określenie ich wysokośći, z krokiem h
3. Krok wyznaczaia punktów h jako parametr
4. Wyznacznie funkcji "h-trace": wysokośći od odległości

#### WEIGHT
**Wymyślić kryteria, obliczania kosztw przelotu**
1. Na podstawie funkcji "h-trace", wyznacznie kosztów przelotu - wag

### SOLVE
**Zastosować algorytm optymalneś ścieżki, np "Dikstra"**
1. Stworzć tabele grafu: Ścieżka, waga
2. Zamplementować algorytm: powinny być biblioteki
