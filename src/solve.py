import sys

class Solve:
	pts = []
	nodes = []
	graph = {}
	log = None;
	def __init__(self, log, points, paths):
		self.log = log;

		self.pts = points
		for idx in range(len(points)):
			self.nodes.append(points[idx][0])

		for node in self.nodes:
			self.graph[node] = {}

		for i in range(len(paths)):
			self.graph[paths[i][0][0]][paths[i][1][0]] = paths[i][2];
			self.graph[paths[i][1][0]][paths[i][0][0]] = paths[i][3];


		self.find_path(0,1)
	def find_path(self, start, target):
		try:
			path = []
			tab = self.dijkstra(self.nodes, self.graph, start)
			res, cost = self.find_target(tab, target);

			for i in range(len(res) - 1):
				beg_idx = self.nodes.index(res[i])
				end_idx = self.nodes.index(res[i+1])
				beg_pt = self.pts[beg_idx]
				end_pt = self.pts[end_idx]
				path.append( (beg_pt, end_pt, 0, 0) )
			return path
		except Exception as e:
			print(e)
			self.log.error(e)





	def dijkstra(self, node, graph, start):
		# init unvisit nodes
		unvisit = node.copy()

		# init tab
		tab = {}
		for n in node:
			tab[n] = (sys.maxsize, None)

		# init start node
		prev_dist = 0;
		current = start;
		tab[start] = (0, None)

		# unvisit current node
		while( len(unvisit) > 0):
			# chcecked distance to neighbor
			for node in graph[current].keys():
				dist = graph[current][node] + prev_dist
				if( dist < tab[node][0] ):
					tab[node] = ( dist, current )

			# change previous node
			prev_node = current;
			# change current node
			neighbor = []
			for n in unvisit:
				neighbor.append( (n, tab[n][0]) )
			current = min(neighbor, key = lambda v: v[1])[0]
			# change previous distance
			prev_dist = tab[current][0]
			# remove node from unvisit nodes
			unvisit.remove(current)


		return tab;

	def find_target(self, tab, target):
		try:
			path = []
			n = target
			while(tab[n][1] != None):
				path.append(n)
				n = tab[n][1]
			path.append(n)
			path.reverse()
			# return path and cost
			return path, tab[target][0]
		except Exception as e:
			print(e)
			self.log.error(e)
