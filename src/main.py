from map import Map
from path import Path
from solve import Solve
import logging
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np



# Parameters #
BEG_PT 		= (0,0)	# Starting point (x,y)
END_PT 		= (32,32)	# Target point (x,y)
NUM_OF_PTS 	= 1000	# Number of generated points
NUM_OF_KNN 	= 5		# Number od connetion to nearest neighbors
TRACE_ACC 	= 0.1	# Trace accurancy (step of htrace)
ALT 		= 100	# Prefer altitute
ALT_OFFSET	= 10	# Altitute offset in colision
RES_DIR 	= "../res/"				# Destyny folder
MAP_DIR 	= "../res/terrain_2.png" 	# Path to 1-channel image representing map

logging.basicConfig(filename = RES_DIR + "app.log", level = logging.INFO)


map = Map(logging, MAP_DIR)
path = Path(logging, map.map, ALT, ALT_OFFSET);
pts = path.createPoints(BEG_PT, END_PT, NUM_OF_PTS);
grp = path.createGraph(NUM_OF_KNN, TRACE_ACC);
solve = Solve(logging, pts, grp);
trace = solve.find_path(0, 1)
trace = path.set_height(trace)
print(trace)
print("=============")

map.createGraph(RES_DIR + "map_graph.png", pts, grp)
map.createGraph(RES_DIR + "map_solve.png", pts, trace)


if( len(trace) > 0 ):
	# plot 3D
	fig = plt.figure()
	ax = plt.axes(projection ='3d')

	x = np.zeros(map.n)
	y = np.zeros(map.m)
	h = np.zeros([map.n, map.m])

	tx = []
	ty = []
	tz = []

	# start node
	node = trace[0]
	tx.append( node[0][1][0] )
	ty.append( node[0][1][1] )
	tz.append( node[2] )
	# next node
	for i in range(0, len(trace)):
		node = trace[i]
		tx.append( node[1][1][0] )
		ty.append( node[1][1][1] )
		tz.append( node[3] )


	for i in range(0, map.n):
		x[i] = i
		for j in range(0, map.m):
			y[j] = j
			h[i][j] = map.map[i][j]

	x, y = np.meshgrid(x, y)
	plt.title('Mapa terenu')
	plt.xlabel('Długość')
	plt.ylabel('Szerokość')
	ax.set_zlabel('Wysokość')
	ax.plot3D(tx, ty, tz, 'red')
	ax.contour3D(x, y, h.transpose(), 75, cmap='viridis')
	# ax.plot_surface(x, y, h.transpose(), cmap ='viridis')
	# ax.plot_wireframe(x, y, h.transpose(), color ='green')
	plt.show()
