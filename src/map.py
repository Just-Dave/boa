import numpy as np
from colorsys import hsv_to_rgb
from PIL import Image, ImageDraw
import matplotlib.pyplot as plt




class Map:
	COLOR_BEST_PATH = (255,0,0)
	COLOR_GRAPH = (128,128,128)
	COLOR_POINT = (0,0,0)
	COLOR_BEG_PT = (0,0,255)
	COLOR_END_PT = (255,0,0)
	SIZE_POINT = 0.2
	SIZE_LINE = 0.1
	log = None
	map = None
	n = 0
	m = 0
	ps = 50			# pixel size

	def __init__(self, log, filename):
		self.log = log

		try:
			img = Image.open(filename)
			if img.mode != "P":
				raise Exception("incorrect image mode")

			self.n, self.m = img.size
			self.map = np.zeros((self.n, self.m))
			for x in range(self.n):
				for y in range(self.m):
					self.map[x][y] = img.getpixel((x,y))
			# DEBUG #
			# print("MAP:")
			# for i in range(self.n):
			# 	print(self.map[:][i])
			# DEBUG #
		except Exception as e:
			self.log.error(e)

	def drawbase(self, draw):
		legendpos = ((self.n+0.5)*self.ps, self.ps)
		self.drawmap(draw)
		self.drawlegend(draw, legendpos )

	def createMap(self, filename):
		try:
			imsize = (self.n * self.ps + 2*self.ps, self.m * self.ps)

			img = Image.new("RGB", imsize, (120, 120, 120))
			draw = ImageDraw.Draw(img)

			self.drawbase(draw)

			img.save(filename)
		except Exception as e:
			print(e)
			self.log.error(e)


	def createPoint(self, filename, points):
		try:
			imsize = (self.n * self.ps + 2*self.ps, self.m * self.ps)

			img = Image.new("RGB", imsize, (120, 120, 120))
			draw = ImageDraw.Draw(img)

			self.drawbase(draw)
			self.drawpoint(draw, points)

			img.save(filename)
		except Exception as e:
			print(e)
			self.log.error(e)

	def createGraph(self, filename, point, graph):
		try:
			imsize = (self.n * self.ps + 2*self.ps, self.m * self.ps)

			img = Image.new("RGB", imsize, (120, 120, 120))
			draw = ImageDraw.Draw(img)

			self.drawbase(draw)
			self.drawline(draw, graph, color=self.COLOR_GRAPH)
			self.drawpoint(draw, point, color=self.COLOR_POINT)

			img.save(filename)
		except Exception as e:
			print(e)
			self.log.error(e)

	def drawpoint(self, draw, point, color = (0,0,0)):
		offset = int(self.SIZE_POINT * self.ps)
		rgb = color;
		for p in point:
			if(p[0] == 0):
				rgb = self.COLOR_BEG_PT
			elif(p[0] == 1):
				rgb = self.COLOR_END_PT
			else:
				rgb = color
			x0 = p[1][0] * self.ps + offset
			y0 = p[1][1] * self.ps + offset
			x1 = x0 + self.ps - 1 - 2 * offset
			y1 = y0 + self.ps - 1 - 2 * offset
			draw.ellipse((x0, y0, x1, y1), rgb)

	def drawline(self, draw, line, color = (0,0,0)):
		if line == None:
			return
		for l in line:
			x0 = l[0][1][0] * self.ps + int(0.5 * self.ps)
			y0 = l[0][1][1] * self.ps + int(0.5 * self.ps)
			x1 = l[1][1][0] * self.ps + int(0.5 * self.ps)
			y1 = l[1][1][1] * self.ps + int(0.5 * self.ps)
			draw.line([x0, y0, x1, y1], color, int(self.SIZE_LINE * self.ps))

	def drawmap(self, draw):
		for i in range(self.n):
			for j in range(self.m):
				x = i*self.ps
				y = j*self.ps
				rect = [x, y, x+self.ps, y+self.ps]
				fill = self.putcolor(self.map[i][j])
				draw.rectangle(rect, fill)

	def drawlegend(self, draw, pos = (0,0), hmin = 0, hmax = 255):
		lvl = 2 * (self.m - 2)
		box = (self.ps, int(self.ps / 2))
		h = int((hmax - hmin) / lvl + 0.5)

		for i in range(lvl):
			x0 = pos[0]
			y0 = pos[1] + box[1] * i
			x1 = x0 + box[0]
			y1 = y0 + box[1]
			color = self.putcolor(hmax - h*i, hmin, hmax)
			draw.rectangle([x0, y0, x1, y1], color)

	def putcolor(self, val, hmin = 0, hmax = 255):
		highHSV = (0, 100, 50)
		lowHSV = (120, 100, 100)

		ratio = val / (hmax - hmin)
		varHSV = (
			(ratio * (highHSV[0] - lowHSV[0]) + lowHSV[0]) / 360,
			(ratio * (highHSV[1] - lowHSV[1]) + lowHSV[1]) / 100,
			(ratio * (highHSV[2] - lowHSV[2]) + lowHSV[2]) / 100)

		varRGB = hsv_to_rgb(varHSV[0], varHSV[1], varHSV[2])
		varRGB = tuple(int(v * 255 + 0.5) for v in varRGB)

		return varRGB

	def createHtrace(self, filename, trace):
		x = []
		y = []
		for p in trace:
			x.append(p[0])
			y.append(p[1])
		plt.figure()
		plt.title("H-Trace")
		plt.xlabel("length")
		plt.ylabel("height")
		plt.plot(x, y)
		plt.savefig(filename)
		plt.close()
















#
