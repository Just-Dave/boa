import random
import math

class Path:
	roof = 50
	roof_offset = 20
	map = None;
	log = None;
	points = [];
	graph = [];
	def __init__(self, log, map, alt, alt_offset):
		self.log = log
		self.map = map
		self.roof = alt
		self.roof_offset = alt_offset

	def createPoints(self, start_pt, target_pt, numOfPoints):
		try:
			range = self.map.shape

			# add start_pt
			if start_pt[0] > range[0] or start_pt[1] > range[1]:
				raise Exception("start point is out of map range")
			self.points.append( (0, (start_pt[0], start_pt[1]) ) )

			# add target_pt
			if target_pt[0] > range[0] or target_pt[1] > range[1]:
				raise Exception("target point is out of map range")
			self.points.append( (1, (target_pt[0], target_pt[1]) ) )

			# check numOfPoints
			if numOfPoints > range[0] * range[1]:
				raise Exception("Too many points generated")


			# Add generated points
			# start in idx 0 (id = 0) and target in idx 1 (id = 1) #
			id = 2;
			while numOfPoints:
				exist = 0;
				x = random.randrange(0, range[0])
				y = random.randrange(0, range[1])

				for p in self.points:
					if p[1][0] == x and p[1][1] == y:
						exist = 1
						break

				if exist == 0:
					self.points.append((id, (x, y)))
					id += 1;
					numOfPoints -= 1;
			# TESTS #
				# self.points = [(0, (4, 12)), (1, (7, 1)), (2, (0, 5)), (3, (1, 2)), (4, (8, 2)), (5, (7, 8)), (6, (6, 8)), (7, (6, 1)), (8, (5, 5)), (9, (10, 11))]
			# TESTS #
			return self.points;
		except Exception as e:
			print(e)
			self.log.error(e)

	def createGraph(self, k, h):
		self.createNeighbors(k)
		for i in range(0, len(self.graph) ):
			htrace = self.hTrace(self.graph[i], h)
			costs = self.cost_1(htrace)
			self.graph[i] = list(self.graph[i])
			self.graph[i][2] = costs[0]
			self.graph[i][3] = costs[1]
		return self.graph


	def createNeighbors(self, k):
		n = len(self.points)
		for ref in range(0, n):
			links = []
			for aim in range(0, n):
				p0 = self.points[ref][1]
				p1 = self.points[aim][1]
				dx = p1[0] - p0[0]
				dy = p1[1] - p0[1]
				length = math.sqrt( dx*dx + dy*dy )
				links.append((self.points[ref], self.points[aim], length))
			## Put kNN to graph
			links = sorted(links, key=lambda x: x[2])
			path = (links[1:k+1])
			for p in path:
				self.graph.append( (p[0], p[1], -1, -1) )
			links.clear()
			path.clear()

	def hTrace(self, path, h):
		try:
			dx = path[1][1][0] - path[0][1][0] #x
			dy = path[1][1][1] - path[0][1][1] #y
			length = math.sqrt(dx*dx + dy*dy)
			direct = (dx/length, dy/length)
			n = int( length / h ) + 1
			h = length / n	# new step

			trace = []
			for i in range(0, n + 1):
				length = i * h;
				x = int(direct[0] * length - 0.5) + path[0][1][0]
				y = int(direct[1] * length - 0.5) + path[0][1][1]
				height = self.map[x][y]
				trace.append( (length, height) )
			return trace
		except Exception as e:
			self.log.error(e)

	def cost_1(self, trace):
		try:
			a = 1
			b = 1.5
			costs = 0
			n = len(trace)
			for i in range(0, n - 1): # a * abs(pktKoncowy.len  - Pkt.poczatkowy.len) + b *abs (pktKoncowy.height  - Pkt.poczatkowy.height)
				if trace[i][1] >= self.roof:
					costs +=  a * abs(trace[i + 1][0] - trace[i][0]) + b * abs(trace[i + 1][1] - trace[i][1])
				else:
					costs +=  a * abs(trace[i + 1][0] - trace[i][0])

			return (costs, costs)
		except Exception as e:
			self.log.error(e)

	def set_height(self, path):
		new_path = []
		for p in path:
			b = p[0][1]
			e = p[1][1]
			h1 = self.map[b[0]][b[1]]
			if h1 >= self.roof:
				h1 = h1 + self.roof_offset;
			else:
				h1 = self.roof

			h2 = self.map[e[0]][e[1]]
			if h2 >= self.roof:
				h2 = h2 + self.roof_offset;
			else:
				h2 = self.roof

			new_path.append( (p[0], p[1], h1, h2) )
		return new_path


	def cost_2(self, path, trace):
		try:
			a = 1
			b = 1.5
			c = 0.4
			costs = 0
			for i in range(0, n - 1):
				costs +=  a * abs(trace[i + 1][0] - trace[i][0]) #+ b * (trace[i + 1][1] - trace[i][1])
				# if (trace[i + 1][1] - trace[i][1]) <
			path[2] = path[3] = costs
		except Exception as e:
			self.log.error(e)

	def cost_3(self, path, trace):
		try:
			a = 1
			b = 1.5
			costs = 0
			for i in range(0, n - 1):
				costs +=  a * abs(trace[i + 1][0] - trace[i][0]) + b * abs(trace[i + 1][1] - trace[i][1])
			path[2] = path[3] = costs
		except Exception as e:
			self.log.error(e)
# 		try:
#
# trace[0] -> (1.2, 3)









#
