import sys

def dijkstra(node, graph, start):
	# init unvisit nodes
	unvisit = node.copy()

	# init tab
	tab = {}
	for n in node:
		tab[n] = (sys.maxsize, None)

	# init start node
	prev_dist = 0;
	current = start;
	tab[start] = (0, None)

	# unvisit current node
	while( len(unvisit) > 0):
		# chcecked distance to neighbor
		for node in graph[current].keys():
			dist = graph[current][node] + prev_dist
			if( dist < tab[node][0] ):
				tab[node] = ( dist, current )

		# change previous node
		prev_node = current;
		# change current node
		neighbor = []
		for n in unvisit:
			neighbor.append( (n, tab[n][0]) )
		current = min(neighbor, key = lambda v: v[1])[0]
		# change previous distance
		prev_dist = tab[current][0]
		# remove node from unvisit nodes
		print(unvisit)
		unvisit.remove(current)

	print("---------")
	for row in tab:
		print(str(row) + ": " + str(tab[row]))

	return tab;

def find_path(tab, target):
	path = []
	n = target
	while(tab[n][1] != None):
		path.append(n)
		n = tab[n][1]
	path.append(n)
	# return path and cost
	return path, tab[target][0]

nodes = [0, 1, 2, 3, 4, 5, 6]
distances = {
    1: {0: 5, 3: 1, 6: 2},
    0: {1: 5, 3: 3, 4: 12, 5 :5},
    3: {1: 1, 6: 1, 4: 1, 0: 3},
    6: {1: 2, 3: 1, 2: 2},
    2: {6: 2, 4: 1, 5: 16},
    4: {0: 12, 3: 1, 2: 1, 5: 2},
    5: {0: 5, 4: 2, 2: 16}}
tab = dijkstra(nodes,distances, 0)
path, cost = find_path(tab, 1)
print(path)
print(cost)
