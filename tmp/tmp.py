import sys

class graph(object):
   def __init__(self, nodes, init_graph):
       self.nodes = nodes
       self.graph = self.construct_graph(nodes, init_graph)

   def construct_graph(self, nodes, init_graph):
       '''Ta metoda zapewnia symetryczność wykresu. Innymi słowy, jeśli istnieje ścieżka z węzła A do B o wartości V,
       musi istnieć ścieżka z węzła B do węzła A o wartości V.
       '''
       graph = {}
       for node in nodes:
           graph[node] = {}

           graph.update(init_graph)

           for node, edges in graph.items():
               for adjacent_node, value in edges.items():
                   if graph[adjacent_node].get(node, False) == False:
                       graph[adjacent_node][node] = value

           return graph

   def get_nodes(self):
       "Returns the nodes of the graph."
       return self.nodes

   def get_outgoing_edges(self, node):
       "Returns the neighbors of a node."
       connections = []
       for out_node in self.nodes:
           if self.graph[node].get(out_node, False) != False:
               connections.append(out_node)
       return connections

   def value(self, node1, node2):
       "Returns the value of an edge between two nodes."
       return self.graph[node1][node2]

def print_result(previous_nodes, shortest_path, start_node, target_node):
   path = []
   node = target_node

   while node != start_node:
       path.append(node)
       node = previous_nodes[node]

   #Dodaj węzeł początkowy ręcznie
   path.append(start_node)

	print("Znaleźliśmy następującą najlepszą ścieżkę o wartości {}.".format(shortest_path[target_node]))
	print(" -> ".join(reversed(path)))

	print(path)

	trace.append( (ps, pe, 0, 0) )
	for i in range(path):

def dijkstra_algoritm(graph, start_node):
   unvisited_nodes = list(graph.get_nodes())

   #Użyjemy tego dyktatu, aby zaoszczędzić na kosztach odwiedzania każdego węzła i aktualizować go,
   # gdy poruszamy się po wykresie
   shortes_path = {}

   #Użyjemy tego dyktatu, aby zapisać najkrótszą znaną ścieżkę do węzła znalezionego do tej pory
   previous_nodes = {}
   #Teraz algorytm może zacząć odwiedzać węzły. Poniższy blok kodu najpierw instruuje algorytm,
   # aby znalazł węzeł o najniższej wartości.

   #Użyjemy max_value, aby zainicjować wartość „nieskończoności” nieodwiedzonych węzłów
   max_value = sys.maxsize
   for node in unvisited_nodes:
       shortes_path[node] = max_value
       #Jednak inicjujemy wartość węzła początkowego z 0
       shortes_path[start_node] = 0

   #Algorytm działa, dopóki nie odwiedzimy wszystkich węzłów
   while unvisited_nodes:
       #Poniższy blok kodu wyszukuje węzeł z najniższym wynikiem
       current_min_node = None
       for node in unvisited_nodes: #Iteruj przez węzły
           if current_min_node == None:
               current_min_node = node
           elif shortes_path[node] < shortes_path[current_min_node]:
               current_min_node = node

       #Poniższy blok kodu pobiera sąsiadów bieżącego węzła i aktualizuje ich odległości
       neighbors = graph.get_outgoing_edges(current_min_node)
       for neighbor in neighbors:
           tentative_value = shortes_path[current_min_node] + graph.value(current_min_node, neighbor)
           if tentative_value < shortes_path[neighbor]:
               shortes_path[neighbor] = tentative_value
               #Aktualizujemy również najlepszą ścieżkę do bieżącego węzła
               previous_nodes[neighbor] = current_min_node

       #Po odwiedzeniu sąsiadów oznaczamy węzeł jako „odwiedzony”
       unvisited_nodes.remove(current_min_node)

   return previous_nodes, shortes_path

nodes = ["Reykjavik", "Oslo", "Moscow", "London", "Rome", "Berlin", "Belgrade", "Athens"]

init_graph = {}
for node in nodes:
   init_graph[node] = {}

init_graph["Reykjavik"]["Oslo"] = 5
init_graph["Reykjavik"]["London"] = 4
init_graph["Oslo"]["Berlin"] = 1
init_graph["Oslo"]["Moscow"] = 3
init_graph["Moscow"]["Belgrade"] = 5
init_graph["Moscow"]["Athens"] = 4
init_graph["Athens"]["Belgrade"] = 1
init_graph["Rome"]["Berlin"] = 2
init_graph["Rome"]["Athens"] = 2
init_graph["Berlin"]["Belgrade"] = 9
init_graph["London"]["Berlin"] = 3

graph = graph(nodes, init_graph)
previous_nodes, shortest_path = dijkstra_algoritm(graph=graph, start_node="Reykjavik")
print(previous_nodes)
print(shortest_path)
print_result(previous_nodes, shortest_path, start_node="Reykjavik", target_node="Belgrade")
